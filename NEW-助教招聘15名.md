&emsp; 



# 连享会：Stata 暑期班-2022

>  Update: `2022.6.6`

&emsp; 

> &#x2B55; [PDF 课纲](https://file.lianxh.cn/KC/lianxh_PX.pdf)  &emsp;  &#x26F3; [往期课程板书和FAQs](https://gitee.com/arlionn/PX/wikis/FAQs/2021%E6%9A%91%E6%9C%9F%E7%8F%AD/readme.md)

&emsp;

## A. 课程概要

> &#x231A; **时间：** 2022 年 7 月 15-25 日  
> &#x2615; **方式：** 网络直播  
> &#x2B50; **授课教师：** 连玉君 (初级+高级) || 孔东民 (论文班)  
> &#x26F5; **报名链接：** <http://junquan18903405450.mikecrm.com/kILLnGa>  
> &#x26EA; **课程主页：** <https://gitee.com/arlionn/PX>  
> &#x26F3; **Note:** 预习资料、常见问题解答等都将通过该主页发布。

>**回放安排**

- **初级班**：7 月 15-17 日 (三天), 网络直播 + 15 天回放
- **高级班**：7 月 19-21 日 (三天), 网络直播 + 15 天回放
- **论文班**：7 月 23-25 日 (三天), 网络直播 + 15 天回放
- **全程班**：7 月 15-25 日, 网络直播 + 45 天回放

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

## &#x26BD; 6. 助教招聘

### 说明和要求

- **名额：** 15 名 (初级、高级和论文班各 5 名)
- **任务：**
  - **A. 课前准备**：协助完成 3 篇介绍 Stata 和计量经济学基础知识的文档；
  - **B. 开课前答疑**：协助学员安装课件和软件，在微信群中回答一些常见问题；
  - **C. 上课期间答疑**：针对前一天学习的内容，在微信群中答疑 (8:00-9:00，19:00-22:00)；
  - Note: 下午 5:30-6:00 的课后答疑由主讲教师负责。
- **要求：** 热心、尽职，熟悉 Stata 的基本语法和常用命令，能对常见问题进行解答和记录  
- **特别说明：** 往期按期完成任务的助教可以直接联系连老师直录。
- **截止时间：** 2022年7月5日 (将于7月6日公布遴选结果于 [课程主页](https://gitee.com/arlionn/PX)，及 连享会主页 [lianxh.cn](https://www.lianxh.cn))

> **申请链接：** <https://www.wjx.top/vj/OT8gdao.aspx>

> 扫码填写助教申请资料：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/助教：2022寒假.png)

> **课程主页：** <https://gitee.com/arlionn/PX>

&emsp;


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;