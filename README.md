&emsp; 



# 连享会：Stata 暑期班-2022

>  Update: `2022.6.6`

&emsp; 

> &#x2B55; [PDF 课纲](https://file.lianxh.cn/KC/lianxh_PX.pdf)  &emsp;  &#x26F3; [往期课程板书和FAQs](https://gitee.com/arlionn/PX/wikis/FAQs/2021%E6%9A%91%E6%9C%9F%E7%8F%AD/readme.md)

&emsp;




## A. 课程概要

> &#x231A; **时间：** 2022 年 7 月 15-25 日  
> &#x2615; **方式：** 网络直播  
> &#x2B50; **授课教师：** 连玉君 (初级+高级) || 孔东民 (论文班)  
> &#x26F5; **报名链接：** <http://junquan18903405450.mikecrm.com/kILLnGa>  
> &#x26EA; **课程主页：** <https://gitee.com/arlionn/PX>  
> &#x26F3; **Note:** 预习资料、常见问题解答等都将通过该主页发布。

>**回放安排**

- **初级班**：7 月 15-17 日 (三天), 网络直播 + 15 天回放
- **高级班**：7 月 19-21 日 (三天), 网络直播 + 15 天回放
- **论文班**：7 月 23-25 日 (三天), 网络直播 + 15 天回放
- **全程班**：7 月 15-25 日, 网络直播 + 45 天回放

&emsp;


## B. 授课嘉宾

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连玉君_2020讲课照180b1.png "连玉君-中山大学")

**[连玉君](http://lingnan.sysu.edu.cn/faculty/lianyujun)**，西安交通大学经济学博士，中山大学岭南学院副教授，博士生导师。已在《China Economic Review》、《经济研究》、《管理世界》、《经济学(季刊)》、《金融研究》、《统计研究》等期刊发表论文 60 余篇。目前已完成 Panel VAR、Panel Threshold、Two-tier Stochastic Frontier 等计量模型的 Stata 实现程序，并编写过几十个小程序，如 `lianxh`, `ihelp`, `sftt`, `winsor2`, `xtbalance`, `bdiff`, `ua` 等。连玉君老师团队一直积极分享 Stata 应用中的经验，开设了 [连享会-主页](https://www.lianxh.cn)，[连享会-直播间](http://lianxh.duanshu.com)，[连享会-知乎](https://www.zhihu.com/people/arlionn) 等专栏，已在微信公众号 (**连享会（ID: lianxh_cn）**) 分享推文 800 余篇，各平台阅读量逾 3000 万人次。

&emsp;

---


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E5%AD%94%E4%B8%9C%E6%B0%91%E5%8D%8A%E8%BA%AB-v2-200.png "孔东民-华中科技大学")

[**孔东民**](http://faculty.hust.edu.cn/kongdongmin)，华中科技大学经济学院教授，博士生导师，中南财经政法大学金融学院文澜特聘教授。主要研究中国资本市场、公司金融、企业行为与发展等。先后入选国家级人才计划 (青年拔尖人才、领军人才)。目前担任International Journal of Finance and Economics (SSCI), Emerging Markets Finance and Trade (SSCI)与China Finance Review International 副主编，Finance Research Letters (SSCI), Economic Modelling (SSCI) 客座编辑。在国内外期刊发表论文近 300 篇，中文成果见诸于《经济研究》, 《管理世界》, 《经济学季刊》, 《金融研究》, 《管理科学学报》, 《中国工业经济》, 《世界经济》等国内权威期刊；英文成果见诸于 Journal of Law and Economics, Management Science, Review of Finance, Contemporary Accounting Research, Journal of Environmental Economics and Management, Journal of Corporate Finance, Journal of Economic Behavior and Organization, Journal of Banking & Finance, Journal of Comparative Economics, Journal of Business Finance & Accounting, Journal of Accounting and Public Policy, China Economic Review, European Accounting Review, Economic Development and Culture Change, Journal of Business Ethics 等期刊。目前为中国管理现代化研究会金融管理专业委员；湖北省金融学会理事，国家自然科学基金通讯评审专家；《证券市场导报》, 《中南财经政法大学学报》, 《会计与经济研究》, 《珞珈管理评论》, 《金融科学》, 《当代会计评论》, 《江汉学术》编委。担任国家社科基金重大项目首席专家，主持了多项国家自然科学基金，并承担财政部与世界银行针对制造业重大问题的联合研究课题以及国开行、建设银行与上交所关于普惠金融和中小投资者保护的联合研究课题。

&emsp;


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)


## C. 本次课程相对于往期课程的变化

- 初级班
  - 新增了「**实证分析可视化**」专题，中间介绍边际效应、分仓散点图、系数差异可视化，以及 Stata 绘图的基本架构
  - 「**模型设定**」专题更新了 70% 以上的内容，包括：条件均值模型的解释、边际效应分析、反事实框架
- 高级班
  - 新增了「假设检验」专题：涉及系数联合检验、内生性检验、稳健性检验、安慰剂检验等主题
  - 新增了「IV 和 GMM」专题，重点介绍 Judge IV 和 Bartik IV。
  - 新增了「面板 ARDL 模型」专题：主要用于分析政策冲击的长期和短期效应，并考虑空间相关性和共同相关结构
  - 新增了「模型均化 (Model Averaging)」专题：主要用于对应对模型不确定性，变量筛选等，是近年来机器学习和稳健性分析的一个新热点
- 论文班
  - 由华中科技大学的孔东民教授主讲，将通过十多篇论文的拆解来论分享学术研究和发表经验，并展示研究设计和方法合理应用的重要性。

&emsp; 

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)


## &#x2B55; 1. Stata 初级班

- **时间：** 2022 年 7 月 15-17 日 (三天)
- **方式：** 网络直播 + 15 天回放
- **授课嘉宾：** 连玉君 (中山大学)
- **授课安排**  
  - **授课方式：** 幻灯片+Stata16/17 实操演示，全程电子板书+Stata 演示截图，课后以 PDF 形式分享给学员  
  - **授课时间：** 上午 9:00-12:00，下午 14:30-17:30 (17:30-18:00 答疑)。  
  - **全程答疑：** 由 10 位经验丰富的同学组成的助教团队会在课程群中全程答疑，并对答疑接龙文档进行详细的记录和分类，公布于 [课程主页](https://gitee.com/arlionn/PX)。
- **课程主页：** <https://gitee.com/arlionn/PX>
- **报名链接：** <http://junquan18903405450.mikecrm.com/kILLnGa>  

&emsp;


## 1.1 课程导引

实证分析中，最伤神和耗时的事情莫过于研究设计和数据处理。在以往的授课中，很多同学和老师都是在听完了高级班的课程以后，又返回头来听初级班的内容。他们有一个共同的感触就是，没有一个扎实的基础，以及对计量经济学和 Stata 整体架构的认识，后续的学习成本会越来越高。

**在初级班中，我力求将三天的课程设置成一个比较完整的体系，目的有二：**

其一，希望大家经过三天的学习（尚需另外花费 1-2 个月的时间演练吸收），能对基本的统计和计量分析方法有所掌握，能读懂多数期刊论文中使用的分析方法；

其二，希望诸位能建立起 Stata 的基本架构，熟知 Stata 能做什么、如何做？以期为后续学习打下宽厚扎实的基础。

翻阅 Top 期刊上的论文，你会发现多数论文并没有使用非常复杂的方法，关键在于论文的想法或视角比较独特，并使用了恰当的方法来论证。这里的关键在于研究设计，而这在目前的计量教科书中鲜有涉及。为此，本次研讨班突出两个特点：一方面，我会努力把基础知识讲解透彻，进度上不求快；另一方面，我在每个专题中都会提供了 2-3 篇比较经典的论文，展示这些方法的合理应用。

在**内容安排**上，基本上遵循了由浅入深，循序渐进的原则。

**第 1-3 讲**依序介绍 Stata 的基本用法、数据处理、程序编写和可视化分析，学习这些内容无需太多的计量经济学基础，但对于提高实证分析能力和分析效率，大有裨益。

**第 4-5 讲**介绍文献中使用频率最高的线性回归模型，包括 OLS 的原理、结果的解释，以及虚拟变量和交乘项的使用等。对于这些内容的深刻理解和熟练掌握，构成了后续，多种主流实证模型的基础，例如，目前文献中广泛使用的固定效应模型 (FE)，倍分法 (DID)，断点回归设计 (RDD) 等方法，本质上就是在传统的线性模型基础上，增加一些虚拟变量或交乘项，配合巧妙的研究设计，来实现对不可观测的个体效应的控制，以及对政策效应的估计。

**第 6 讲**介绍固定效应模型 (FE)，涉及基本的 FE，TWFE 模型和进阶的高维固定效应模型、交互固定效应模型。本讲是第 4 讲和第 5 讲内容的延伸和应用，是目前解决遗漏变量和内生性问题比较常用的方法，也是理解多期 DID，动态面板和面板门槛等进阶模型的基础。 

**具体说明如下：**

在**第 1-2 讲**中，我会以一篇文章为实例，说明 Stata 的基本语法结构，并对数据处理过程中的关键问题进行介绍，如离群值的处理、文字变量的处理等。就我个人的经验而言，数据处理能力的高低直接决定实证分析的效率，而对于离群值等问题的处理是否妥善会直接影响全文结果的稳健性，是多数人不够重视但却至关重要的问题。此前有不少学完了高级班的同学又回炉初级班，便是感悟到了这一点。

**第 3 讲**介绍 Stata 编程的基础知识。但凡提及写程序，很多人都会产生恐惧心理，其实，一旦掌握了最基本的原理和语法格式，Stata 中的程序设定并没有想象的那么困难。更为重要的是，对于多数人而言，由于并不需要写完整的 ado 文档，因此只需要学会最基本的条件语句和循环语句即可，难度又会进一步降低。一旦掌握了基本的编程知识和理念，你的实证分析便开始进入「快车道」了。

**第 4 讲和第 5 讲**介绍实证分析中的模型设定和结果解释问题。很多人会觉得 OLS 很简单，但 Top 期刊中使用最多的仍然是 OLS，如何合理的构建模型、解释结果便成为实证分析中必须掌握的。我精选了大家经常面临的几个专题并结合论文进行讲解，包括：虚拟变量的使用、交乘项的使用和解释、分组回归的合理设定和假设检验。我会终点强调对条件期望函数和 FWL 定理的解读，这构成了理解因果模型、面板模型以及机器学习中多种方法的基础。首经贸的一个博士生发信息给我：「连老师，我发现只要把你初级里面的虚拟变量相关的知识完全掌握，很多复杂的方法就都好理解了，甚至可以自己解决问题。」，我的回复是：「那看来你是把相关的东西基本搞明白了，我每次上初级班的时候会花很多时间讲虚拟变量和交乘项，这构成了双重差分、断点回归、时间中断分析、面板数据模型等一系列模型的重要基础。」

**第 6 讲**介绍了目前广泛应用的 **面板数据模型**。由于面板资料的获取越来越方便，目前多数研究中使用的都是面板数据。在讲解这些模型的基本思想和估计方法的过程中，笔者会将重点放在模型含义和应用范围上来。例如，对于同一笔数据而言，何时采用 OLS 进行估计，何时采用 FE 估计？不同的方法之间有何差异和关联？结果背后的经济含义如何解读？掌握这些方法有助于大家合理控制内生性问题，以便得到更为可信的结论。

**第 7 讲** 会拆解一篇发表于 QJE 的论文。该文基本上涵盖了第 1-6 讲的主要内容。我们可以尝试用新的方法来研究文中涉及的问题。在此过程中，既能巩固对现有模型和方法的理解，也能够对比后续文献来确定新的研究主题。 

## 1.2 专题介绍（Stata 初级班）

> ### A1. Stata 简介

- 实证分析的基本流程和编程习惯
- 数据的导入和导出
- 执行命令和基本统计分析
- 基本统计量的呈现
- 基本统计分析
- do 文件和 log 文件的使用
- 帮助文件的使用和外部命令的安装
- 一篇范例文档

> ### A2. 数据处理和程序

- 数据的横向合并和纵向追加
- 重复样本值、缺漏值和离群值的处理
- 文字变量的处理 
- 局域暂元和全局暂元（local, global）
- 控制语句（条件语句、循环语句）
- Stata 中的各类函数
- Matrix 和 Mata

> ### A3. 实证分析可视化  

- 为什要可视化？
- Stata 绘图命令的架构
- 直方图与密度函数图：`histogram`, `kdensity`, `biplot`
- 分仓散点图：`binscatter`，`binscatter2`
- 系数及系数差异的可视化呈现：`coefplot`
- 调节效应、倒 U 型关系及边际效应的可视化
- 面板数据、多个控制变量、高维固定效应模型的可视化
- 长期与短期关系的可视化
- 范文：2 篇

> ### A4. 线性回归分析  

- 条件期望函数：OLS, MLE 与 MM 的关系
- 线性概率模型
- OLS 估计和系数含义
- FWL 定理 (Frisch-Waugh-Lovell)
- 假设检验和统计推断
- 稳健性标准误：Bootstrap、Jackknife、聚类调整
- 结果输出与呈现

> ### A5. 模型设定和解释  

- 控制变量：选取、含义、可视化
- 变量缩放
- 取对数：弹性与半弹性
- 虚拟变量与固定效应
- 交乘项、平方项、高阶项与调节效应
- 因子变量与边际效应分析
- DID, RDD 与 RKD

> ### A6. 静态面板数据模型

- 何谓个体效应？FE v.s. RE
- 高维固定效应模型
- 长差分 (long difference)
- 交互固定效应模型
- 异方差和序列相关（Bootstrap、Cluster 调整标准误）
- 面板模型中的非时变变量和宏观变量如何分析?
- 实证分析中的常见问题

> ### A7. 一篇 Top 期刊论文重现  

- [Akcigit](http://www.ufukakcigit.com/), U., J. Grigsby, T. Nicholas, S. Stantcheva, **2022**, Taxation and innovation in the twentieth century, **The Quarterly Journal of Economics**, 137 (1): 329-385. [-Link-](https://doi.org/10.1093/qje/qjab022), [-PDF-](https://file-lianxh.oss-cn-shenzhen.aliyuncs.com/Refs/Paper2022/Akcigit-2022-QJE.pdf), [-Appendix-](https://oup.silverchair-cdn.com/oup/backfile/Content_public/Journal/qje/137/1/10.1093_qje_qjab022/1/qjab022_onlineappendix.pdf?Expires=1649965153&Signature=soPrpxU1GGqhdNr58Nc6j-gZhttwWtj2XQG5WxaVp-k7ZqVAJOoYz60biLwCcpgYVpVutAw-uJn59pJkQJOuZlMv6DHHRPiIHE2I7CUNOv5c05r1msRmBbFmzntnyXBov2UkywbuJpob1e59Q5fesu5Z7t6RQFOoh8qgVxjlQcTNgcN6YFuFISMPa2GP8zRbQcNxcFuKbRhPyoUMqFI-MJkwVS7pfl162hJ0ZRa0fH9ho7N3FhBGyoN0jAufE1S3vCSeb2FetG7lhS8JGYMb~FMOcpyRpv1hjSiSL52lSl5W2jT18i1uN-k4atVdt3TN-JFWXMk796qn~BvYnM~85Q__&Key-Pair-Id=APKAIE5G5CRDK6RD3PGA), [-cited-](https://xs2.dailyheadlines.cc/scholar?cites=3691559864871282829&as_sdt=2005&sciodt=0,5&hl=zh-CN&scioq=Social+ties+and+the+selection+of+China%27s+political+elite), [-Replication-](https://doi.org/10.7910/DVN/SR410I)
- **简介：** 文章研究了美国公司税和个人税对创新的影响。作者将发明人数据库、公司税率数据库，以及州级个人所得税和其他经济数据关联起来，从宏观和微观两个层面估计了税收对创新 (数量、质量、发生地等) 的影响。文中采用了多种识别策略，得到了非常一致的结论：(1) 高税率对创新的数量和发生地具有负面影响，但不会影响平均创新质量；(2) 州级层面的「税收-创新产出」弹性很大；(3) 公司税主要影响受雇发明者 (相对于自由职业者) 的创新产出和跨州流动性；而个人所得税则会对整体创新数量和发明人的流动性产生影响。
- 方法：
  - 高维固定效应、长差分、交互固定效应
  - 实证结果可视化：分仓散点图、长期效应
  - 交乘项
- Note: 我只挑选一些与 A1-A5 相关的内容来讲解。

> ### A8. 自行研读-提供复现数据和代码
- Sherman M G, Tookes H E. Female representation in the academic finance profession. **Journal of Finance**, 2022, 77(1): 317-365. [-Link-](https://onlinelibrary.wiley.com/doi/10.1111/jofi.13094), [-cited-](https://xs2.dailyheadlines.cc/scholar?cites=14484989866429574740&as_sdt=2005&sciodt=0,5&hl=zh-CN), [-PDF-](https://onlinelibrary.wiley.com/doi/epdf/10.1111/jofi.13094), [-Replication-](https://onlinelibrary.wiley.com/action/downloadSupplement?doi=10.1111%2Fjofi.13094&file=jofi13094-sup-0002-ReplicationCode.zip)
- 该文研究了金融学术圈中的性别失衡现象。2009-2017 年，美国排名前 100 的商学院中的金融教师中，女性仅占 16.0%。性别失衡表现在几个方面：其一，在控制了研究能力后，更多的女性在排名较低的机构中任职，晋升为正教授的可行性相对较低，伴以薪酬较低。其二，女性发表的论文数量较少，但质量上不存在差异。其三，女性多与同性合作，表明她们的社会关系网较小。时间序列数据表明，上述性别差距正在缩小。
- 该文没有使用任何复杂的回归方法，仅使用了固定效应模型。但在统计分析和结果可视化方面做了很多工作，是 Stata 入门学习的绝佳范本。
- 方法：
  - 各种统计分析，列表和图形呈现：`egen`, `foreach` 
  - OLS, 高维固定效应模型, 交乘项, 因子变量：`reghdfe`
  - 结果可视化：`coefplot`
  - 结果输出：`estadd`, `estout`, `esttab`, 

> **温馨提示：** 开课前请自学如下内容

- a. 预习: [连玉君公开课：Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0)，报名后赠送电子课件 (数据和 dofiles)
- b. 预习: [连玉君公开课-直击面板数据](https://lianxh.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e) (面板模型概述)，以便预习。
- c. 预习: [连玉君 B 站视频：Stata程序的编写和发布](https://www.bilibili.com/video/BV1XQ4y117Xa)，Stata 程序基础
- d. 预习: [连享会公开课：计量基础及 Stata 应用](https://www.lianxh.cn/news/ab26038e5d3dd.html)

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20210528162133.png)

&emsp;

<div STYLE="page-break-after: always;"></div>

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp; 


## &#x2B55; 2. Stata 高级班

- **时间：** 2022 年 7 月 19-21 日 (三天)
- **方式：** 网络直播 + 15 天回放
- **授课嘉宾：** 连玉君 (中山大学)
- **授课安排**  
  - **授课方式：** 幻灯片+Stata16/17 实操演示，全程电子板书+Stata 演示截图，课后以 PDF 形式分享给学员  
  - **授课时间：** 上午 9:00-12:00，下午 14:30-17:30 (17:30-18:00 答疑)。  
  - **全程答疑：** 由 10 位经验丰富的同学组成的助教团队会在课程群中全程答疑，并对答疑接龙文档进行详细的记录和分类，公布于 [课程主页](https://gitee.com/arlionn/PX)。
- **课程主页：** <https://gitee.com/arlionn/PX>
- **报名链接：** <http://junquan18903405450.mikecrm.com/kILLnGa> 

&emsp;


## 2.1 课程导引

**Stata 高级班** 是初级班的进一步深入，也为理解 **论文班** 的十多篇论文提供了方法上的铺垫，涵盖目前主流分析方法和研究设计框架。高级班采用「庖丁解牛」的方式讲解目前 Top 期刊中使用的计量方法和模型，而 **论文班** 则展示多种方法的巧妙组合，突出「研究设计」的重要性。


## 2.2 专题介绍（Stata 高级班）

> ### B1. 实证分析中各类检验方法
> 近期的 Top 期刊越来越强调模型不确定性，比如：控制变量也有好坏之分、是否存在非线性特征、不同模型的优劣对比等。这就需要进行各类检验，以便排除各种「混杂因素」和「似是而非」的论述，让论文的研究结论具有排他性，经济含义也更为清晰明确。本专题包括假设检验的基本原理、模型筛选和对比检验，以及「不容易做好」的稳健性检验等内容。在介绍检验方法和命令的同时，重点在于如何解释它们的经济含义，如何选择合适的检验方法并采用合适的方式加以呈现和分析。
- 系数的联合检验：Wald，LR，LM 检验
  - `test`, `testparm`, `lincom`, `nlcom`, `testnl`
  - 结果的汇集与呈现
- 模型比较：嵌套模型比较、非嵌套模型比较
- R2 分解和贡献度分析
- 系数差异检验：Chow 检验，SUR，Bootstrap，排序检验
- 内生性检验
- 稳健性检验、安慰剂检验
- 参考文献：
  - Hansen B E . 2021. Econometrics. Princeton University Press. [Data and Contents](https://www.ssc.wisc.edu/~bhansen/econometrics/), [PDF](https://www.ssc.wisc.edu/~bhansen/econometrics/Econometrics.pdf). Chap 9.

> ### B2. IV 和 GMM 估计
> IV 的思想并不复杂，但想找到一个能说服审稿人的 IV 却往往是可遇不可求的事情。在横截面分析中确实如此。随着面板数据模型的快速发展，IV 的构造思路已经发生了很大的变化，「就地取材」、「差别反应」都是非常有用的构造思路。本讲在介绍 IV 和 GMM 的原理基础上，重点介绍 Judge IV 和 Bartik IV 的构造和应用方法。 
- IV 和 2SLS 估计的原理
- GMM 估计的原理
- 工具变量合理性检验
- 弱工具变量问题
- Judge IV 和 Jackknife IV
- Bartik (shift-share) IV 
- 参考文献：
  - Hansen B E . 2021. Econometrics. Princeton University Press. [Data and Contents](https://www.ssc.wisc.edu/~bhansen/econometrics/), [PDF](https://www.ssc.wisc.edu/~bhansen/econometrics/Econometrics.pdf). Chap 12-13.
  - Borusyak, K., P. Hull, X. Jaravel, **2022**, Quasi-experimental shift-share research designs, **Review of Economic Studies**, 89 (1): 181-213. [-Link-](https://doi.org/10.1093/restud/rdab030), [-PDF-](https://sci-hub.ren/10.1093/restud/rdab030)
  - Adao, R., M. Kolesar, E. Morales, **2019**, Shift-share designs: Theory and inference, **Quarterly Journal of Economics**, 134 (4): 1949-2010. [-Link-](https://doi.org/10.1093/qje/qjz025), [-PDF-](https://sci-hub.ren/10.1093/qje/qjz025)
  - Goldsmith-Pinkham, P., I. Sorkin, H. Swift, **2020**, Bartik instruments: What, when. Why, and how, **American Economic Review**, 110 (8): 2586-2624. [-Link-](https://doi.org/10.1257/aer.20181047), [-PDF-](https://sci-hub.ren/10.1257/aer.20181047)

> ### B3a. 动态面板模型
> 多数经济行为的调整都会因为「调整成本」的存在而表现出粘性，即只能实现部分调整；同时，即使基于理性预期模型进行决策，但由于信息存在滞后，变量在时序上就会表现出较强的相关性。这些都构成了动态关系的理论基础，也使得动态面板模型广泛出现于经济增长、公司金融、国际贸易、劳动经济学等领域。本讲介绍该模型的估计和检验方法，尤其是实际分析中的几个主要陷阱。
- 一阶差分 GMM 估计（FD-GMM）
- 系统 GMM 估计 (SYS-GMM)
- 序列相关检验
- 过度识别检验（Sargan 检验）
- 模型设定常见问题(弱工具变量问题)
- 参考文献：
  - Roodman, David. **2009**, How to do Xtabond2: An Introduction to Difference and System GMM in Stata, **Stata Journal**, 9(1): 86–136. [-PDF-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900106)
  - Richard Williams, Paul D. Allison, Enrique Moral-Benito, **2018**, Linear Dynamic Panel-data Estimation Using Maximum Likelihood and Structural Equation Modeling, **Stata Journal**, 18(2): 293–326. [-PDF-](https://sci-hub.ren/10.1177/1536867X1801800201), [-PDF2-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800201)

> ### B3b. 面板 VAR 模型
> 面板 VAR 模型可以视为多变量动态面板模型，多用于估计和检验几个内生变量的动态关系。基于 IRF 和 FEVD，我们也可以进行预测。该模型在经济增长、能源、财政、创新等领域有广泛应用。
- VAR 模型简介
- 冲击反应函数 (IRF)
- 方差分解 (FEVD)
- 面板 Granger 检验
- 允许外生变量的 PVAR 模型
- 应用实例（介绍 2 篇论文）
- 参考文献：
  - Michael R. M. Abrigo, I. Love, **2016**, Estimation of Panel Vector Autoregression in Stata, **Stata Journal**, 16(3): 778–804. [-PDF-](https://sci-hub.ren/10.1177/1536867X1601600314), [-PDF2-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1601600314), [-cited-](https://xs2.dailyheadlines.cc/scholar?cites=18404893275718129284&as_sdt=2005&sciodt=0,5&hl=zh-CN)
  - Acheampong, A. O., **2018**, Economic growth, co2 emissions and energy consumption: What causes what and where?, **Energy Economics**, 74: 677-692. [-Link-](https://doi.org/https://doi.org/10.1016/j.eneco.2018.07.022), [-PDF1-](https://sci-hub.ren/https://doi.org/10.1016/j.eneco.2018.07.022), [-Replication-](https://ars.els-cdn.com/content/image/1-s2.0-S014098831830272X-mmc1.zip)  

> ### B4. 面板 ARDL 模型

面板 ARDL 模型全称「面板自回归分布滞后模型」，主要用于估计变量之间的长期关系。在时间序列分析中，该模型主要用于分析具有协整关系的非平稳序列之间的长期和短期关系。在面板数据中，我们可以更好地控制各类固定效应、考虑空间相关以及异质性特征，以便分析一项政策或某个变化缓慢的变量 (如气候、制度) 对经济增长、创新、贸易等结果变量的影响。本主题介绍面板 ARDL 模型的主要设定形式，以及该模型在经济和金融领域的应用场景。
- 时间序列简介：AR 与 MA 过程
- ARDL 模型：AutoRegression Distributed Lag 
- 部分调整模型与理性预期模型
- 面板 ARDL 模型
- 长期乘数与短期效应
- Stata 范例：Akcigit, U., J. Grigsby, T. Nicholas, S. Stantcheva, **2022**, Taxation and innovation in the twentieth century, **Quarterly Journal of Economics**, 137 (1): 329-385. [-Link-](https://doi.org/10.1093/qje/qjab022), [-PDF-](https://file-lianxh.oss-cn-shenzhen.aliyuncs.com/Refs/Paper2022/Akcigit-2022-QJE.pdf), [-Replication-](https://doi.org/10.7910/DVN/SR410I)
- 扩展阅读：
  - Dell, M., B. F. Jones, B. A. Olken, **2012**, Temperature shocks and economic growth: Evidence from the last half century, **American Economic Journal-Macroeconomics**, 4 (3): 66-95. [-Link-](https://doi.org/10.1257/mac.4.3.66), [-PDF-](https://sci-hub.ren/10.1257/mac.4.3.66), [-Replication-](https://www.openicpsr.org/openicpsr/project/114251/version/V1/view)
  - Kahn, M. E., K. Mohaddes, R. N. C. Ng, M. H. Pesaran, M. Raissi,J.-C. Yang, 2021, Long-term macroeconomic effects of climate change: A cross-country analysis, Energy Economics, 104: 105624. [-Link-](https://doi.org/10.1016/j.eneco.2021.105624), [-PDF1-](https://sci-hub.ren/10.1016/j.eneco.2021.105624), [-PDF2-](https://www.nber.org/system/files/working_papers/w26167/w26167.pdf), [-Replication-](http://dx.doi.org/10.17632/hytzz8wftw), [Cited](https://xs2.dailyheadlines.cc/scholar?cites=6240333152230290872&as_sdt=2005&sciodt=0,5&hl=zh-CN). `lincom`, `xtmg`
  - Ditzen, J. 2021. "Estimating long-run effects and the exponent of cross-sectional dependence: An update to xtdcce2". The Stata Journal: Promoting communications on statistics and Stata, 21 (3): 687-707. [Link](https://doi.org/10.1177/1536867x211045560), [PDF1](http://sci-hub.ren/10.1177/1536867x211045560). [PDF2](http://pro1.unibz.it/projects/economics/repec/bemps81.pdf).

> ### B5. 截面和面板门槛模型
> 实证分析中，经常要处理结构变化问题，目前主要使用交乘项和分组回归等方式，但这两种设定方法都需要预先知道或假设结构变化点，使其合理性颇受质疑。本讲介绍的面板门槛模型则基于「让数据说话」的原则，自动搜索结构变化点，从而克服了上述方法的局限。
- Bootstrap 简介
- **截面**门槛模型（Cross-sectional Threshold Model）
- **面板**门槛模型（Panel Threshold Model）
- **动态面板**门槛模型（Dynamic Panel Threshold Mode）
- 参考文献：
  - ​
  - Chudik, A., K. Mohaddes, M. H. Pesaran, M. Raissi, **2017**, Is there a debt-threshold effect on output growth?, **Review of Economics and Statistics**, 99 (1): 135-150. [-Link-](https://doi.org/10.1162/REST_a_00593), [-PDF-](https://sci-hub.ren/10.1162/REST_a_00593)
  - Seo, M. H., S. Kim, Y. J. Kim, **2019**, Estimation of dynamic panel threshold model using stata, **Stata Journal**, 19 (3): 685-697. [-Link-](https://doi.org/10.1177/1536867x19874243), [-PDF-](https://sci-hub.ren/10.1177/1536867x19874243)
  - Wang, Q., 2015, Fixed-Effect Panel Threshold Model using Stata, Stata Journal, 15(1): 121–134. [-PDF-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500108)

> ### B6. 模型平均化 (Model Averaging)
> 模型平均方法 (Model Averaging，MA)，是当今国际统计学和计量经济学研究的热点之一，实质是一类集成学习方法。其基本思想是把多种模型的结果进行加权平均，以便降低模型不确定性，提高预测效果。其核心研究内容是权重的选择以及不确定性的衡量。本专题讲解 MA 的基本思想和两类基本方法：weighted-average least-squares (WALS)；Bayesian model-averaging (BMA)，以及它们在经济和金融领域的应用。
- 模型不确定性
- 模型筛选：BIC，AIC，best subset，交叉验证
- WALS：加权平均最小二乘法
- BMA：贝叶斯模型均化
- 参考文献：
  - Feng, Y., Q. F. Liu, Q. S. Yao, G. Q. Zhao, **2021**, Model averaging for nonlinear regression models, **Journal of Business & Economic Statistics**: 1-14. [-Link-](https://doi.org/10.1080/07350015.2020.1870477), [-PDF1-](https://sci-hub.ren/10.1080/07350015.2020.1870477)
  - Breuer, M., H. H. Schutt, **2021**, Accounting for uncertainty: An application of bayesian methods to accruals models, **Review of Accounting Studies**: 1-43. [-Link-](https://doi.org/10.1007/s11142-021-09654-0), [-PDF1-](https://sci-hub.ren/10.1007/s11142-021-09654-0)
  - Steel, M. F. J., **2020**, Model averaging and its use in economics, **Journal of Economic Literature**, 58 (3): 644-719. [-Link-](https://doi.org/10.1257/jel.20191385), [-PDF1-](https://sci-hub.ren/10.1257/jel.20191385)
  - Magnus, J. R., G. De Luca, **2016**, Weighted-average least squares (wals): A survey, **Journal of Economic Surveys**, 30 (1): 117-148. [-Link-](https://doi.org/10.1111/joes.12094), [-PDF1-](https://sci-hub.ren/10.1111/joes.12094)
  - Fletcher, D. 2018, **Model averaging**, Springer Berlin Heidelberg, Berlin, Heidelberg. [-Link-](https://doi.org/10.1007/978-3-662-58541-2_1), [-PDF1-](https://sci-hub.ren/10.1007/978-3-662-58541-2_1), [-PDF-](https://link.springer.com/content/pdf/10.1007/978-3-662-58541-2.pdf). **Text Book**.

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20211203014058.png)

&emsp;

<div STYLE="page-break-after: always;"></div>

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp; 

## &#x2B55; 3. Stata 论文班

- **授课嘉宾：** 孔东民 (华中科技大学)
- **时间：** 2022 年 7 月 23-25 日 (三天)
- **方式：** 网络直播 + 15 天回放
- **授课安排**  
  - **授课方式：** 幻灯片+Stata16/17 实操演示，全程电子板书+Stata 演示截图，课后以 PDF 形式分享给学员  
  - **授课时间：** 上午 9:00-12:00，下午 14:30-17:30 (17:30-18:00 答疑)。  
  - **全程答疑：** 由 10 位经验丰富的同学组成的助教团队会在课程群中全程答疑，并对答疑接龙文档进行详细的记录和分类，公布于 [课程主页](https://gitee.com/arlionn/PX)。
- **课程主页：** <https://gitee.com/arlionn/PX>
- **报名链接：** <http://junquan18903405450.mikecrm.com/kILLnGa> 


&emsp; 

## 3.1 课程导引

如何更为顺利的把自己的 idea 和研究结果转变为可以发表的论文？是寻求更好更全面的数据？还是更多的结果展示？或者更复杂前沿的计量方法？如果有答案的话，可能都不全对。经济学论文的价值在很大程度上取决于你探索和解决的问题如何更好的弥补前人的研究或者回答我们所关注的问题，与之相伴的是一系列逻辑严密分析和相适宜的研究设计。研读已经发表的论文通常只能看到作者研究工作的最终成果，而期间的思考、分析和打磨过程，则不得而知，而这恰恰是多数人更应该首先学习和琢磨的。

为此，在论文班的三天中，孔老师将分享论文选题与写作的基本要点，并结合交叉学科与前沿研究对此进行深入探讨。与此同时，嘉宾通过精讲自己发表于国内外重要期刊的多篇论文，介绍如何讲好中国故事，并讲解投稿过程中审稿人关注的问题以及如何根据审稿人意见有效修正研究设计，以帮助论文写作与发表。在此基础上，进一步结合相关领域目前的研究状态，探讨未来可能的研究方向。课程中涉及不同的研究设计和估计方法的综合应用，包括：混合 OLS、固定效应模型、双重差分法 (DID)、断点回归 (RDD)、聚束分析法、地理断点分析、安慰剂检验、稳健性检验等。

## 3.2. 专题介绍 (Stata 论文班)

> ### C1. Idea 的寻找与选择

- **A.**  新变量
  - 如何寻找新变量，发现新关系？
  - 如何引入更好的代理变量？
- **B.**  新方法与新技术
  - 新方法与旧结果如何有效结合？
  - 文本分析与机器学习如何运用？
- **C.**  新数据
  - 如何寻找与使用独特的数据？
- **D.**  新视角
  - 如何基于实证进行扩展，提出新维度、新市场或新的跨国证据？
  - 如何引入市场/地区层面的变量？
  - 如何做到学科交叉？
- **E.**  政策评估
  - 当前重大经济政治事件的原因与后果如何分析？
- **主要文献**
  - Bertrand, M., and S. Mullainathan, 2003, Enjoying the quiet life? Corporate governance and managerial preferences, ***Journal of Political Economy*** 111, 1043-1075. [-Link-](https://www.journals.uchicago.edu/doi/abs/10.1086/376950), [--PDF](https://sci-hub.hkvisa.net/10.1086/376950)
  - Giroud, X., and H. M. Mueller, 2010, Does corporate governance matter in competitive industries?, ***Journal of Financial Economics*** 95, 312-331. [-Link-](https://www.sciencedirect.com/science/article/pii/S0304405X09002293?casa_token=x0roANteRn0AAAAA:t8dQcDt3gLLo-Zg1jtipXiRCl5pVgWWHTXRz-cznWC1jvzzqrayQXyvi6sHf5yYjaCsjiAQ3AQ), [-PDF-](https://sci-hub.hkvisa.net/10.1016/j.jfineco.2009.10.008)
  - Henderson, J. V., A. Storeygard, and D. N. Weil, 2012, Measuring economic growth from outer space, ***American Economic Review*** 102, 994-1028. [-Link-](https://www.aeaweb.org/articles?id=10.1257/aer.102.2.994), [-PDF-](https://sci-hub.hkvisa.net/10.1257/aer.102.2.994)
  - Michalopoulos, S., and E. Papaioannou, 2014, National institutions and subnational development in Africa, **Quarterly Journal of Economics** 129, 151-213. [-Link-](https://academic.oup.com/qje/article/129/1/151/1897929?login=true), [-PDF-](https://sci-hub.hkvisa.net/10.1093/qje/qjt029)

> ### C2. 研究设计、因果识别与方法讨论

- **主题：**
  - OLS与内生性
  - IV+DID+RDD 串讲
  - 聚束分析法 (Bunching)
  - 地理断点
- **主要文献**
  - Roberts, Michael R., and Toni M. Whited.  Endogeneity in empirical corporate finance1. ***Handbook of the Economics of Finance***. Vol. 2. Elsevier, 2013. 493-572.  [-Link-](https://www.sciencedirect.com/science/article/pii/B9780444535948000070?casa_token=RhkShdsGrSgAAAAA:d4f0D2yp9fVdCzIclpW45nN9oFPRORJt2_VgRt7xaAqDA0Iu5antpIzJ9xG-Xnzd2pKTeLy7AIk) [-PDF-](https://sci-hub.hkvisa.net/10.1016/B978-0-44-453594-8.00007-0)
  - Dittmar, Jeremiah E. Information technology and economic change: the impact of the printing press. ***The Quarterly Journal of Economics*** 126.3 (2011): 1133-1172. [-Link-](https://academic.oup.com/qje/article/126/3/1133/1855353?login=true), [-PDF-](https://sci-hub.hkvisa.net/10.1093/qje/qjr035)
  - Chetty, Raj, John N. Friedman, Tore Olsen, and Luigi Pistaferri, 2011, Adjustment costs, firm responses, and micro vs. Macro labor supply elasticities: Evidence from Danish tax records, ***The Quarterly Journal of Economics*** 126, 749-804. [-Link-](https://academic.oup.com/qje/article/126/2/749/1870394?login=true), [-PDF-](https://sci-hub.hkvisa.net/10.1093/qje/qjr013)
  - Devereux, Michael P., Li Liu, and Simon Loretz, 2014, The elasticity of corporate taxable income: New evidence from uk tax records, ***American Economic Journal: Economic Policy*** 6, 19-53. [-Link-](https://www.aeaweb.org/articles?id=10.1257/pol.6.2.19), [-PDF-](https://sci-hub.hkvisa.net/10.1257/pol.6.2.19)
  - Chava, S., and M. R. Roberts, 2008, How does financing impact investment? The role of debt covenants, ***Journal of Finance*** 63, 2085-2121. [-Link-](https://onlinelibrary.wiley.com/doi/full/10.1111/j.1540-6261.2008.01391.x?casa_token=MJkcTKVby4MAAAAA%3Ah_IIl7kjYH7_N-I35j7CYLIa7f144vqW2H9XgIs9y9P12GbWT-56o0VOWpCuezFMcoOEVqbWOVS1mAer), [-PDF-](https://sci-hub.hkvisa.net/https://doi.org/10.1111/j.1540-6261.2008.01391.x)

> ### C3. 学科交叉与研究前沿：基于企业行为的讨论

- 视角1：劳动经济学
- 视角2：国际贸易
- 视角3：气候与环境
- 视角4：文化与历史
- 视角5：非理性与人类行为
- **主要文献**
  - Benmelech, Efraim, Nittai Bergman, and Amit Seru, 2021, Financing labor, ***Review of Finance*** 25, 1365-1393. [-Link-](https://academic.oup.com/rof/article/25/5/1365/6275768?login=true), [-PDF-](https://sci-hub.hkvisa.net/10.1093/rof/rfab013)
  - Hong, Harrison, G. Andrew Karolyi, and José A. Scheinkman. Climate finance. ***The Review of Financial Studies*** 33.3 (2020): 1011-1023. [-Link-](https://academic.oup.com/rfs/article/33/3/1011/5735309?login=true), [-PDF-](https://sci-hub.hkvisa.net/10.1093/rfs/hhz146)
  - Karolyi, G. Andrew. The gravity of culture for finance. ***Journal of Corporate Finance*** 41 (2016): 610-625.[-Link-](https://www.sciencedirect.com/science/article/pii/S0929119916300761?casa_token=3bSIR1U2wXAAAAAA:AQnT9_9zOxcwughgFZT26PPA1tN3i4saS1Dj54yyko16eSoyuHLnotLGVSFvAg8bemuiflmS6Vc), [-PDF-](https://sci-hub.hkvisa.net/10.1016/j.jcorpfin.2016.07.003)
  - Shefrin, Hersh. Behavioral corporate finance. ***Journal of applied corporate finance*** 14.3 (2001): 113-126.[-Link-](https://onlinelibrary.wiley.com/doi/full/10.1111/j.1745-6622.2001.tb00443.x?casa_token=w2INofjw0o0AAAAA%3AHh-yaioiy4an5wlNtMoGTpZ1FedNr_o-YCExooVlk04a9Czq6EeOwvWwLKA6-Q1BysoQftlukTN8BQ_w), [-PDF-](https://sci-hub.hkvisa.net/https://doi.org/10.1111/j.1745-6622.2001.tb00443.x)
  - Whited, T. M., 2019, JFE special issue on labor and finance, ***Journal of Financial Economics*** 133, 539-540. [-Link-](https://ideas.repec.org/a/eee/jfinec/v133y2019i3p539-540.html), [-PDF-](https://sci-hub.hkvisa.net/10.1016/j.jfineco.2019.06.010)

> ### C4. 论文投稿与审稿人和编辑关注的问题

- 如何投稿？
- 审稿人关注什么？
- 匿名审稿过程
- 如何应对审稿意见
- 文章为何会被拒？
- 论文发表周期
- 一些被拒稿的经验
- 作为SSCI期刊的编辑，处理稿件时关注的问题


> ### C5. 几篇中国话题论文的缘起、修改与发表

**主要文献**
  - Kong and Qin, 2021, China's anti-corruption campaign and entrepreneurship. ***Journal of Law and Economics***, 64(1). [-Link-](https://www.journals.uchicago.edu/doi/abs/10.1086/711313), [-PDF-](https://sci-hub.hkvisa.net/10.1086/711313)
  - Han, Kong, and Liu, 2018, Do Analysts Gain an Informational Advantage by Visiting Listed Companies, ***Contemporary Accounting Research***, 35(4): 1843-1867. [-Link-](https://onlinelibrary.wiley.com/doi/full/10.1111/1911-3846.12363?casa_token=cCqhzsaFdpAAAAAA%3AWkdg2DKzmlMydxFUv_NHhwBhT2yQCpRxUBYOZWtweloS5E3XH5U2lGmfY77vKDF9a-QLLv37akb33zwWbA), [-PDF-](https://sci-hub.hkvisa.net/https://doi.org/10.1111/1911-3846.12363)
  - Zhang, Qi, and Kong, 2019, The Real Effects of Legal Institutions: Environmental Courts and Firm-level Environmental Protection Expenditure. ***Journal of Environmental Economics and Management,*** 98, 102254. [-Link-](https://www.sciencedirect.com/science/article/pii/S0095069618303693?casa_token=-16zGv06OlcAAAAA:n1uNTwmQJbYCgsHVonUdtlGynMpHGa6ltDiWhcdQkysiRnpR63coiDvVSeI7y5AULPTkjGnSsvvb), [-PDF-](https://sci-hub.hkvisa.net/10.1016/j.jeem.2019.102254)
  - 孔高文, 刘莎莎, 孔东民. 机器人与就业：基于行业与地区异质性的探索性分析, 中国工业经济, 2020(8): 80-98. [-Link-](https://kns.cnki.net/kcms/detail/detail.aspx?dbcode=CJFD&dbname=CJFDLAST2020&filename=GGYY202008008&uniplatform=NZKPT&v=iD_WqMyZ0GetvoN5-bMHCL5fpvUTrCqLUaTjDOkB7PKvb35C3QR1s4ZrHQyDrXxm)
  - 孔东民, 刘莎莎. 中小股东投票权、公司决策与公司治理, 管理世界，2017(9): 101-115. [-Link-](https://kns.cnki.net/kcms/detail/detail.aspx?dbcode=CJFD&dbname=CJFDLAST2018&filename=GLSJ201709010&uniplatform=NZKPT&v=rwawR7h5Y7fDiNOMHkVqjJ9cGVk-aOqAZr2HJQx8o-P6REyZWQff4qdOCInfJmfk)

&emsp;

### 最后的话

需要特别强调的是，自我提升从来都不是件轻松的事情。因此，在开课之前，大家务必认真研读每一篇论文，了解其研究背景、研究思路、计量方法和主要结论，带着问题听课。同时，也建议大家在开课前务必掌握文献的检索方法，学会使用微软学术、谷歌学术和 **Endnote** 等工具，以便追踪每篇论文的后续进展，发掘新的研究主题。


&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20210528163014.png)

&emsp;

<div STYLE="page-break-after: always;"></div>

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp; 

## &#x23F3; 4. 报名和缴费信息

- **主办方：** 太原君泉教育咨询有限公司
- **标准费用**(初级/高级/论文班 单班报名)：3700 元/班/人 
- **优惠方案**：
  - **专题课老学员单班报名：** 9 折，3330 元/人
  - **学生（需提供学生证/卡照片）**：9 折，3330 元/人
  - **会员单班报名：** 85折，3145 元/人
  - **三班任意两班组合报名：** 6100 元/人
  - **全程班报名：** 9000 元/人

- **温馨提示：** 以上各项优惠不能叠加使用。
- **联系方式：**
  - 邮箱：[wjx004@sina.com](wjx004@sina.com)
  - 王老师：18903405450 (微信同号)
  - 李老师：18636102467 (微信同号)


&emsp;

> **报名链接：** [http://junquan18903405450.mikecrm.com/kILLnGa](http://junquan18903405450.mikecrm.com/kILLnGa)

> &#x23E9; 长按/扫描二维码报名：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/报名：2022寒假班.png)

### 缴费方式

> **方式 1：对公转账**

- 户名：太原君泉教育咨询有限公司
- 账号：35117530000023891 (山西省太原市晋商银行南中环支行)
- **温馨提示：** 对公转账时，请务必提供「**汇款人姓名-单位**」信息，以便确认。

> **方式 2：扫码支付**

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/君泉收款码.png)

> **温馨提示：** 

- 扫码支付后，请将「**付款记录**」截屏发给王老师-18903405450（微信同号）

&emsp;

<div STYLE="page-break-after: always;"></div>

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp; 

## &#x26F3; 5. 听课指南

### 5.1 软件和课件
- **课件/计量软件：** Stata，提供全套 Stata 实操程序、数据和 dofiles。建议使用 Stata 16.0 或更高版本。
- **听课软件**：本次课程可以在手机，ipad ，平板以及 windows/Mac 系统的电脑上听课。

> #### **特别提示：**

- 为保护讲师的知识产权和您的账户安全，系统会自动在您观看的视频中嵌入您的「用户名」信息
- 一个账号绑定一个设备，且听课电脑不能外接显示屏，请大家提前准备好自己的听课设备。
- 本课程为虚拟产品，**一经报名，不得退换**。 
  
### 5.2 实名制报名
本次课程实行实名参与，具体要求如下：   
- 高校老师/同学报名时需要向连享会课程负责人 **提供真实姓名，并附教师证/学生证图片**；
- 研究所及其他单位报名需提供 **能够证明姓名以及工作单位的证明**；
- 报名即默认同意「[<font color=darkred>**连享会版权保护协议条款**</font>](https://www.lianxh.cn/news/b16b512ee620b.html)」。

&emsp;

<div STYLE="page-break-after: always;"></div>

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp; 

## &#x26BD; 6. 助教招聘

### 说明和要求

- **名额：** 15 名 (初级、高级和论文班各 5 名)
- **任务：**
  - **A. 课前准备**：协助完成 3 篇介绍 Stata 和计量经济学基础知识的文档；
  - **B. 开课前答疑**：协助学员安装课件和软件，在微信群中回答一些常见问题；
  - **C. 上课期间答疑**：针对前一天学习的内容，在微信群中答疑 (8:00-9:00，19:00-22:00)；
  - Note: 下午 5:30-6:00 的课后答疑由主讲教师负责。
- **要求：** 热心、尽职，熟悉 Stata 的基本语法和常用命令，能对常见问题进行解答和记录  
- **特别说明：** 往期按期完成任务的助教可以直接联系连老师直录。
- **截止时间：** 2022年7月5日 (将于7月6日公布遴选结果于 [课程主页](https://gitee.com/arlionn/PX)，及 连享会主页 [lianxh.cn](https://www.lianxh.cn))

> **申请链接：** <https://www.wjx.top/vj/OT8gdao.aspx>

> 扫码填写助教申请资料：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/助教：2022寒假.png)

> **课程主页：** <https://gitee.com/arlionn/PX>

&emsp;


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;


## 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，700+ 推文，实证分析不再抓狂。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- &#x26F3; [课程主页](https://www.lianxh.cn/news/46917f1076104.html)